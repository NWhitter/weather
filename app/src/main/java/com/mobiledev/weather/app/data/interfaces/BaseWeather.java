package com.mobiledev.weather.app.data.interfaces;

import java.io.Serializable;

/**
 * @author Natasha Whitter
 * @since 20/05/2014
 */
public abstract class BaseWeather implements Serializable{
    protected int id;
    protected double cod;
    protected String message;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getCod() {
        return cod;
    }

    public void setCod(double cod) {
        this.cod = cod;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "BaseWeather{" +
                "id=" + id +
                ", cod=" + cod +
                ", message='" + message + '\'' +
                '}';
    }
}
