package com.mobiledev.weather.app.network;

import android.os.AsyncTask;
import android.util.Log;
import com.mobiledev.weather.app.data.CurrentWeather;
import com.mobiledev.weather.app.network.interfaces.AsyncWidget;
import com.mobiledev.weather.app.network.utils.ParserUtils;

/**
 * @author Natasha Whitter
 * @since 21/05/2014
 */
public class WidgetWeatherParser extends AsyncTask<Double, Void, CurrentWeather>
{
    private static final String TAG = "CurrentWeatherParser";
    public static AsyncWidget response = null;

    public static CurrentWeather getWeather(double longitude, double latitude)
    {
        return ParserUtils.getCurrentWeather(new WeatherConnection().getCurrentWeather(longitude, latitude));
    }

    @Override
    protected CurrentWeather doInBackground(Double... params) {

        CurrentWeather weather = getWeather(params[0], params[1]);
        Log.d(TAG, weather.toString());
        return weather;
    }

    @Override
    protected void onPostExecute(CurrentWeather weather) {
        if (response != null) {
            response.processFinish(weather);
        }
    }
}
