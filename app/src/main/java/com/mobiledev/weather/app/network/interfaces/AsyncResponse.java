package com.mobiledev.weather.app.network.interfaces;

import com.mobiledev.weather.app.data.CurrentWeather;
import com.mobiledev.weather.app.data.DailyForecastWeather;
import com.mobiledev.weather.app.data.HourlyForecastWeather;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 10/04/2014
 */
public interface AsyncResponse
{
    void processFinish(CurrentWeather weather);
    void processFinish(DailyForecastWeather weather);
    void processFinish(HourlyForecastWeather weather);
}

