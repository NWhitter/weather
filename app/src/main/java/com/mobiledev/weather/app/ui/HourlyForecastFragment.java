package com.mobiledev.weather.app.ui;

import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.*;
import android.widget.AbsListView;
import android.widget.AdapterView;
import com.mobiledev.weather.app.R;
import com.mobiledev.weather.app.data.HourlyForecast;
import com.mobiledev.weather.app.data.HourlyForecastWeather;
import com.mobiledev.weather.app.network.AlarmService;
import com.mobiledev.weather.app.ui.adapter.HourlyForecastAdapter;

/**
 * @author Natasha Whitter
 * @since 18/05/2014
 */
public class HourlyForecastFragment extends ListFragment implements AdapterView.OnItemClickListener
{
    public static final String TAG = "HourlyForecastFragment";
    public static int id = 0;
    public HourlyForecastAdapter adapter;
    public HourlyForecastWeather forecast;
    public Context context;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        id = this.getId();
        context = getActivity();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_refresh, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.action_refresh:
                context.startService(new Intent(getActivity(), AlarmService.class));
                return true;
            case R.id.action_settings:
                Intent intent = new Intent(context, PrefsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_city:
                Intent intent1 = new Intent(context, SelectCityActivity.class);
                startActivity(intent1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Intent intent = new Intent(getActivity(), AlarmService.class);
        intent.putExtra(context.getString(R.string.pref_weather_type), 2);
        context.startService(intent);
        getListView().setOnItemClickListener(this);
        getListView().setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        getListView().setSelector(android.R.color.darker_gray);
    }

    @Override
    public void onPause() {
        super.onPause();
        context.stopService(new Intent(getActivity(), AlarmService.class));
    }

    public void processFinish(HourlyForecastWeather weather) {
        if (weather.getCod() == 200)
        {
            forecast = weather;
            adapter = new HourlyForecastAdapter(context, weather);
            setListAdapter(adapter);
            Log.d(TAG, weather.toString());
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(getString(R.string.intent_forecast), (HourlyForecast) parent.getItemAtPosition(position));
        bundle.putString(getString(R.string.intent_city), forecast.location.getCity());
        bundle.putString(getString(R.string.intent_country), forecast.location.getCountry());
        bundle.putLong(getString(R.string.intent_sunrise), forecast.location.getSunrise());
        bundle.putLong(getString(R.string.intent_sunset), forecast.location.getSunset());

        Intent intent = new Intent(context, HourlyWeatherFragment.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
