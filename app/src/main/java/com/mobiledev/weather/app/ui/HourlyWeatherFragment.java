package com.mobiledev.weather.app.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mobiledev.weather.app.ApplicationConstants;
import com.mobiledev.weather.app.R;
import com.mobiledev.weather.app.data.HourlyForecast;
import com.mobiledev.weather.app.utils.WeatherUtils;

import java.text.SimpleDateFormat;
import java.text.StringCharacterIterator;
import java.util.Date;

/**
 * @author Natasha Whitter
 * @since 20/05/2014
 */
public class HourlyWeatherFragment extends Activity
{

private RelativeLayout rlBackground;
    private TextView tvCity;
    private TextView tvCountry;
    private ImageView ivWeatherIcon;
    private TextView tvTemp;
    private TextView tvWindSpeed;
    private TextView tvWindDegree;
    private TextView tvClouds;
    private TextView tvHumidity;
    private TextView tvPressure;
    private TextView tvSunrise;
    private TextView tvSunset;
    private TextView tvMaxTemp;
    private TextView tvMinTemp;
    private TextView tvRain;
    private TextView tvSnow;
    private TextView tvLastUpdated;
    private ImageView ivDetectLocation;
    private TextView tvLastUpdatedTitle;

    private HourlyForecast weather;
    private SharedPreferences sharedPref;

    private String city;
    private String country;
    private long sunrise;
    private long sunset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getActionBar() != null)
        {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }

        setContentView(R.layout.activity_current_weather);

        tvCity = (TextView) findViewById(R.id.current_city);
        tvCountry = (TextView) findViewById(R.id.current_country);
        ivWeatherIcon = (ImageView) findViewById(R.id.current_image);
        tvTemp = (TextView) findViewById(R.id.current_temp);
        tvWindSpeed = (TextView) findViewById(R.id.wind_speed);
        tvWindDegree = (TextView) findViewById(R.id.wind_direction);
        tvClouds = (TextView) findViewById(R.id.cloud_coverage);
        tvHumidity = (TextView) findViewById(R.id.humidity);
        tvPressure = (TextView) findViewById(R.id.pressure);
        tvSunrise = (TextView) findViewById(R.id.sunrise);
        tvSunset = (TextView) findViewById(R.id.sunset);
        tvMaxTemp = (TextView) findViewById(R.id.max_temp);
        tvMinTemp = (TextView) findViewById(R.id.mini_temp);
        tvRain = (TextView) findViewById(R.id.rain);
        tvSnow = (TextView) findViewById(R.id.snow);
        tvLastUpdated = (TextView) findViewById(R.id.last_updated);
        ivDetectLocation = (ImageView) findViewById(R.id.gps_picture);
        tvLastUpdatedTitle = (TextView) findViewById(R.id.last_updated_title);
        rlBackground = (RelativeLayout) findViewById(R.id.list);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        ivDetectLocation.setVisibility(View.GONE);
        tvLastUpdated.setVisibility(View.GONE);
        if (tvLastUpdatedTitle != null) {
            tvLastUpdatedTitle.setVisibility(View.GONE);
        }

        Intent intent = getIntent();

        if (intent != null)
        {
            Bundle bundle = intent.getExtras();
            weather = (HourlyForecast) bundle.getSerializable(getString(R.string.intent_forecast));
            city = bundle.getString(getString(R.string.intent_city));
            country = bundle.getString(getString(R.string.intent_country));
            sunrise = bundle.getLong(getString(R.string.intent_sunrise), 0);
            sunset = bundle.getLong(getString(R.string.intent_sunset), 0);
            initGui();
        }
        else
        {
            Toast.makeText(this, "Forecast data couldn't be received", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initGui()
    {
        if (weather != null)
        {
            rlBackground.setBackgroundColor(WeatherUtils.getBackgroundColor(weather.temperature.getTemperature(), this));

            tvCity.setText(city);
            tvCountry.setText(country);
            tvTemp.setText(WeatherUtils.getTemperature(weather.temperature.getTemperature(), this));
            ivWeatherIcon.setImageDrawable(getResources().getDrawable(WeatherUtils.getIcon(weather.condition.getIcon())));
            tvWindSpeed.setText(WeatherUtils.getWindSpeed(weather.wind.getSpeed(), this));
            tvWindDegree.setText(WeatherUtils.getDegrees(weather.wind.getDegree()));
            tvClouds.setText(Integer.toString(weather.clouds.getAll()) + "%");
            tvHumidity.setText(Integer.toString((int) Math.round(weather.condition.getHumidity())) + "%");
            tvPressure.setText(Integer.toString(WeatherUtils.getPressure(weather.condition.getPressure(), this)));

            SimpleDateFormat minuteHour = new SimpleDateFormat(ApplicationConstants.DEFAULT_TIME_12HR, getResources().getConfiguration().locale);
            tvSunrise.setText(minuteHour.format(new Date(sunrise)));
            tvSunset.setText(minuteHour.format(new Date(sunset)));

            tvMaxTemp.setText(WeatherUtils.getTemperature(weather.temperature.getMaxTemperature(), this));
            tvMinTemp.setText(WeatherUtils.getTemperature(weather.temperature.getMinTemperature(), this));
            tvRain.setText(Integer.toString((int) Math.round(weather.rain.getAmount())));
            tvSnow.setText(Integer.toString((int) Math.round(weather.snow.getAmount())));
        }
    }
}
