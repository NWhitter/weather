package com.mobiledev.weather.app.utils;

import android.location.Location;
import android.util.Log;
import com.mobiledev.weather.app.ApplicationConstants;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Natasha Whitter
 * @since 10/05/2014
 */
public class LocationUtils
{
    private static final String TAG = "LocationUtils";
    private static final int time = 20000;

    /**
     * Checks if the new location update is better than the previous location update
     * @param location - new location
     * @param currentLocation - old location
     * @return boolean depending if the new location is better than the old location
     * @see "http://stackoverflow.com/questions/19891788/how-onlocationchange-and-requestlocationupdate-link-together"
     */
    public static boolean isBestLocation(Location location, Location currentLocation)
    {
        // If there's no current location, then set new location
        if (currentLocation == null) {
            return true;
        }

        long timeDiff = location.getTime() - currentLocation.getTime();
        float accuracyDiff = location.getAccuracy() - currentLocation.getAccuracy();

        boolean isNewer = timeDiff > time;
        boolean isOlder = timeDiff < time;
        boolean isLessAccurate = accuracyDiff > 0;
        boolean isMoreAccurate = accuracyDiff < 0;

        if (isNewer)
        {
            return true;
        }else if (isOlder)
        {
            return false;
        } else if (isMoreAccurate)
        {
            return true;
        }

        return isNewer && !isLessAccurate || isNewer && !isLessAccurate && isSameProvider(location.getProvider(), currentLocation.getProvider());

    }

    private static boolean isSameProvider(String provider, String currentProvider)
    {
        if (provider == null)
        {
            return currentProvider == null;
        }
        return provider.equals(currentProvider);
    }

    /**
     * This method uses the string received from the calling method to get places matching the string entered,
     * received JSON data is parsed to an array of places.
     * @param input - string to be send to get places matching
     * @return results received from places
     * @see "https://developers.google.com/places/training/autocomplete-android"
     */
    public static ArrayList<String> autocomplete(String input)
    {
        ArrayList<String> results = null;

        HttpURLConnection connection = null;
        StringBuilder stringBuilder = new StringBuilder();

        try
        {
            StringBuilder builder = new StringBuilder(ApplicationConstants.PLACES_BASE_URL + ApplicationConstants.PLACES_AUTOCOMPLETE + ApplicationConstants.PLACES_JSON);
            builder.append(ApplicationConstants.PLACES_SENSOR);
            builder.append(ApplicationConstants.PLACES_CITY_TYPE);
            builder.append(ApplicationConstants.PLACES_COMPONENTS);
            builder.append(ApplicationConstants.PLACES_LANG_ENG);
            builder.append(ApplicationConstants.PLACES_INPUT_TYPE);
            builder.append(URLEncoder.encode(input, "utf8"));
            builder.append(ApplicationConstants.PLACES_API_KEY);

            Log.d(TAG, builder.toString());

            URL url = new URL(builder.toString());
            connection = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(connection.getInputStream());

            // Put data into stringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1)
            {
                stringBuilder.append(buff, 0, read);
                Log.d("LU", stringBuilder.toString());
            }
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) { connection.disconnect(); }
        }

        try
        {
            // Create a JSON array from the stringBuilder object
            JSONArray array = new JSONObject(stringBuilder.toString()).getJSONArray("predictions");

            // Extract the Place descriptions
            results = new ArrayList<String>(array.length());
            for (int i = 0; i < array.length(); i++)
            {
                results.add(array.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return results;
    }

    public static boolean isToday(long receivedTime)
    {
        if (receivedTime == 0) { return true; }

        Calendar today = Calendar.getInstance();
        Calendar updated = Calendar.getInstance();
        updated.setTime(new Date(receivedTime));

        return (today.get(Calendar.YEAR) == updated.get(Calendar.YEAR))
                && (today.get(Calendar.MONTH) == updated.get(Calendar.MONTH))
                && (today.get(Calendar.DAY_OF_MONTH) == updated.get(Calendar.DAY_OF_MONTH));
    }
}
