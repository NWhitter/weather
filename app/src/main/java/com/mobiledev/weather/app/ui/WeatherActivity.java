package com.mobiledev.weather.app.ui;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import com.mobiledev.weather.app.R;
import com.mobiledev.weather.app.data.CurrentWeather;
import com.mobiledev.weather.app.data.DailyForecastWeather;
import com.mobiledev.weather.app.data.HourlyForecastWeather;
import com.mobiledev.weather.app.network.AlarmService;
import com.mobiledev.weather.app.network.CurrentWeatherParser;
import com.mobiledev.weather.app.network.DailyForecastParser;
import com.mobiledev.weather.app.network.HourlyForecastParser;
import com.mobiledev.weather.app.network.interfaces.AsyncResponse;
import com.mobiledev.weather.app.ui.adapter.WeatherPagerAdapter;
import com.mobiledev.weather.app.ui.CurrentWeatherFragment;
import com.mobiledev.weather.app.ui.DailyForecastFragment;
import com.mobiledev.weather.app.ui.HourlyForecastFragment;

/**
 * @author Natasha Whitter
 * @since 20/05/2014
 */
public class WeatherActivity extends FragmentActivity implements AsyncResponse{
    private ViewPager pager;
    private WeatherPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewpager);

        if (getActionBar() != null)
        {
            getActionBar().setTitle(getString(R.string.app_name));
        }

        pager = (ViewPager) findViewById(R.id.viewpager);
        adapter = new WeatherPagerAdapter(getFragmentManager());
        pager.setAdapter(adapter);

        CurrentWeatherParser.response = WeatherActivity.this;
        DailyForecastParser.response = WeatherActivity.this;
        HourlyForecastParser.response = WeatherActivity.this;
    }

    /**
     * Checks if the user wants to close the application.
     * If yes, then the application will close.
     * If no, then the application will stay open.
     */
    @Override
    public void onBackPressed()
    {
        new AlertDialog.Builder(this).setTitle("Leaving " + getResources().getString(R.string.app_name))
                .setMessage("Are you sure you would like to leave?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        // To access the super, I have to access the class and call OnBackPressed
                        WeatherActivity.super.onBackPressed();
                    }
                })
                        // Button doesn't need to do anything
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void processFinish(CurrentWeather weather) {
        try {
            CurrentWeatherFragment weatherFragment = (CurrentWeatherFragment) getFragmentManager().findFragmentById(CurrentWeatherFragment.id);
            weatherFragment.processFinish(weather);
        } catch (ClassCastException e) {
            e.printStackTrace();
            if (isAlarmRunning()) {
                stopService(new Intent(this, AlarmService.class));
            }
        }
    }

    @Override
    public void processFinish(DailyForecastWeather weather) {
        try {
            DailyForecastFragment forecastFragment = (DailyForecastFragment) getFragmentManager().findFragmentById(DailyForecastFragment.id);
            forecastFragment.processFinish(weather);
        } catch (ClassCastException e) {
            e.printStackTrace();
            if (isAlarmRunning()) {
                stopService(new Intent(this, AlarmService.class));
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopService(new Intent(this, AlarmService.class));
    }

    @Override
    public void processFinish(HourlyForecastWeather weather) {
        try {
            HourlyForecastFragment forecastFragment = (HourlyForecastFragment) getFragmentManager().findFragmentById(HourlyForecastFragment.id);
            forecastFragment.processFinish(weather);
        } catch (ClassCastException e) {
            e.printStackTrace();
            if (isAlarmRunning()) {
                stopService(new Intent(this, AlarmService.class));
            }
        }
    }

    private boolean isAlarmRunning()
    {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for(ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE))
        {
            if (AlarmService.class.getName().equals(service.service.getClassName()))
            {
                return true;
            }
        }
        return false;
    }
}
