package com.mobiledev.weather.app.data.interfaces;

import java.io.Serializable;

/**
 * @author Natasha Whitter
 * @since 20/05/2014
 */
public abstract class BaseLocation implements Serializable {
    protected int id;
    protected double longitude;
    protected double latitude;
    protected long sunrise;
    protected long sunset;
    protected String city;
    protected String country;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public long getSunrise() {
        return sunrise;
    }

    public void setSunrise(long sunrise) {
        this.sunrise = sunrise;
    }

    public long getSunset() {
        return sunset;
    }

    public void setSunset(long sunset) {
        this.sunset = sunset;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "BaseLocation{" +
                "id=" + id +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", sunrise=" + sunrise +
                ", sunset=" + sunset +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
