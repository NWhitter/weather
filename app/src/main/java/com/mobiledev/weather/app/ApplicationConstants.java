package com.mobiledev.weather.app;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * @author Natasha Whitter
 * @since 09/05/2014
 */
public class ApplicationConstants {

    // OpenWeatherMap URL constants
    public static final String BASE_URL = "http://api.openweathermap.org/data/2.5/weather?";
    public static final String BASE_DAILY_FORECAST_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?";
    public static final String BASE_FORECAST_URL = "http://api.openweathermap.org/data/2.5/forecast?";
    public static final String CITY_QUERY = "q=";
    public static final String LAT_QUERY = "lat=";
    public static final String LON_QUERY = "&lon=";
    public static final String DAY_QUERY = "&cnt=14";
    public static final String JSON_MODE = "&mode=json";
    public static final String LANG_ENG = "&lang=en";

    // Google Places URL constants
    public static final String PLACES_BASE_URL = "https://maps.googleapis.com/maps/api/place";
    public static final String PLACES_AUTOCOMPLETE = "/autocomplete";
    public static final String PLACES_JSON = "/json";
    public static final String PLACES_INPUT_TYPE = "&input=";
    public static final String PLACES_COMPONENTS = "&components=country:uk";
    public static final String PLACES_SENSOR = "?sensor=false";
    public static final String PLACES_CITY_TYPE = "&types=(cities)";
    public static final String PLACES_LANG_ENG = "&language=en";
    public static final String PLACES_API_KEY = "&key=AIzaSyAsLM3kqz9o_l06uC7n4YQ5QQYgKB6iQw0";

    // date and time formats
    public static String DEFAULT_DATE = "dd/MM/yyyy";
    public static String SHORT_DATE = "dd/MM/yy";
    public static String LONG_DATE = "dd:MMM:yyyy";

    public static String DEFAULT_DATETIME = "dd/MM/yyyy HH:mm:ss";
    public static String SHORT_DATETIME = "dd/MM/yy HH:mm:ss";
    public static String LONG_DATETIME = "dd/MMM/yyyy HH:mm:ss";
    public static String LONG_DATETIME_A = "yyyy-MM-dd HH:mm:ss";

    public static String SHORT_TIME = "HH:mm:ss";
    public static String DEFAULT_TIME_24HR = "HH:mm";
    public static String DEFAULT_TIME_12HR = "hh:mm a";
    public static String LONG_TIME = "HH:mm:ss:mm";

    /**
     * Checks if the device is connected to the internet
     * @param context - context from calling class
     * @return boolean stating if an internet connection is available
     */
    public static boolean isNetworkConnected(Context context)
    {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected());
    }
}
