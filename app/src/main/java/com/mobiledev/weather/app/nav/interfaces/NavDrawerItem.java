package com.mobiledev.weather.app.nav.interfaces;

public interface NavDrawerItem
{
    public int getId();

    public String getLabel();

    public int getType();

    public boolean isEnabled();

    public boolean updateActionBarTitle();
}
