package com.mobiledev.weather.app.nav;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import com.mobiledev.weather.app.data.DailyForecastWeather;
import com.mobiledev.weather.app.data.HourlyForecastWeather;
import com.mobiledev.weather.app.network.*;
import com.mobiledev.weather.app.R;
import com.mobiledev.weather.app.data.CurrentWeather;
import com.mobiledev.weather.app.network.interfaces.AsyncResponse;
import com.mobiledev.weather.app.ui.CurrentWeatherFragment;
import com.mobiledev.weather.app.ui.DailyForecastFragment;
import com.mobiledev.weather.app.ui.HourlyForecastFragment;

/**
 * @author Nathan Esquenazi <Nathan Esquenazi>
 * @version 1.0
 * @since 21/02/2014
 */

public class NavigationDrawer extends FragmentActivity implements AsyncResponse
{
    private FragmentNavigationDrawer drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        PreferenceManager.setDefaultValues(this, R.xml.refresh_prefs, false);
        setContentView(R.layout.drawer_navigation);

        drawer = (FragmentNavigationDrawer) findViewById(R.id.drawer_layout);
        drawer.setupDrawerConfiguration((ListView) findViewById(R.id.drawer_list),  R.id.drawer_frame);

        // Fragments to be added in the navigation drawer
        drawer.addNavItem("Current Weather", "Weather", CurrentWeatherFragment.class);
        drawer.addNavItem("Daily Forecast", "Daily Forecast", DailyForecastFragment.class);
        drawer.addNavItem("Hourly Forecast", "Hourly Forecast", HourlyForecastFragment.class);

        CurrentWeatherParser.response = NavigationDrawer.this;
        DailyForecastParser.response = NavigationDrawer.this;
        HourlyForecastParser.response = NavigationDrawer.this;

        if (savedInstanceState == null) {
            drawer.selectDrawerItem(0);
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        return drawer.getDrawerToggle().onOptionsItemSelected(item) || super.onOptionsItemSelected(item);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState)
    {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawer.getDrawerToggle().syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawer.getDrawerToggle().onConfigurationChanged(newConfig);
    }

    /**
     * Checks if the user wants to close the application.
     * If yes, then the application will close.
     * If no, then the application will stay open.
     */
    @Override
    public void onBackPressed()
    {
        new AlertDialog.Builder(this).setTitle("Leaving " + getResources().getString(R.string.app_name))
                .setMessage("Are you sure you would like to leave?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        // To access the super, I have to access the class and call OnBackPressed
                        NavigationDrawer.super.onBackPressed();
                    }
                })
                        // Button doesn't need to do anything
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void processFinish(CurrentWeather weather) {
        try {
            CurrentWeatherFragment weatherFragment = (CurrentWeatherFragment) getFragmentManager().findFragmentById(CurrentWeatherFragment.id);
            weatherFragment.processFinish(weather);
        } catch (ClassCastException e) {
            e.printStackTrace();
            if (isAlarmRunning()) {
                stopService(new Intent(this, AlarmService.class));
            }
        }
    }

    @Override
    public void processFinish(DailyForecastWeather weather) {
        try {
            DailyForecastFragment forecastFragment = (DailyForecastFragment) getFragmentManager().findFragmentById(DailyForecastFragment.id);
            forecastFragment.processFinish(weather);
        } catch (ClassCastException e) {
            e.printStackTrace();
            if (isAlarmRunning()) {
                stopService(new Intent(this, AlarmService.class));
            }
        }
    }

    @Override
    public void processFinish(HourlyForecastWeather weather) {
        try {
            HourlyForecastFragment forecastFragment = (HourlyForecastFragment) getFragmentManager().findFragmentById(HourlyForecastFragment.id);
            forecastFragment.processFinish(weather);
        } catch (ClassCastException e) {
            e.printStackTrace();
            if (isAlarmRunning()) {
                stopService(new Intent(this, AlarmService.class));
            }
        }
    }

    private boolean isAlarmRunning()
    {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for(ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE))
        {
            if (AlarmService.class.getName().equals(service.service.getClassName()))
            {
                return true;
            }
        }
        return false;
    }
}
