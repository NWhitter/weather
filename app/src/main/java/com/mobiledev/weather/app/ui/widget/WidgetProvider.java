package com.mobiledev.weather.app.ui.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;
import android.widget.RemoteViews;
import com.mobiledev.weather.app.R;
import com.mobiledev.weather.app.data.CurrentWeather;
import com.mobiledev.weather.app.network.AlarmService;
import com.mobiledev.weather.app.network.WidgetWeatherParser;
import com.mobiledev.weather.app.network.interfaces.AsyncWidget;
import com.mobiledev.weather.app.utils.WeatherUtils;


/**
 * Implementation of App Widget functionality.
 * http://www.vogella.com/tutorials/AndroidWidgets/article.html#overview_appwidgetprovider
 */
public class WidgetProvider extends AppWidgetProvider implements AsyncWidget {
    private CurrentWeather weather;
    private Context context;
    private int[] ids;


    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        ComponentName widget = new ComponentName(context, AppWidgetProvider.class);
        ids = appWidgetManager.getAppWidgetIds(widget);
    }


    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
        this.context = context;
        WidgetWeatherParser.response = WidgetProvider.this;
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        boolean autoUpdate = sharedPref.getBoolean(context.getString(R.string.pref_auto_update_key), true);
        int updateFrequency = Integer.parseInt(sharedPref.getString(context.getString(R.string.pref_widget_update_key), "0"));
        if (autoUpdate || updateFrequency != -1) {
            Intent intent = new Intent(context, AlarmService.class);
            intent.putExtra(context.getString(R.string.pref_weather_type), 3);
            context.startService(intent);
        }
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
        context.stopService(new Intent(context, AlarmService.class));
    }

    @Override
    public void processFinish(CurrentWeather weather) {
        AppWidgetManager manager = AppWidgetManager.getInstance(context.getApplicationContext());
        ComponentName widget = new ComponentName(context.getApplicationContext(), WidgetProvider.class);
        int[] id2 = manager.getAppWidgetIds(widget);

        for (int widgetId : id2)
        {
            RemoteViews views = new RemoteViews(context.getApplicationContext().getPackageName(), R.layout.widget_current_weather);
            views.setTextViewText(R.id.widget_town, weather.location.getCity());
            views.setImageViewBitmap(R.id.weather_logo, ((BitmapDrawable) context.getResources().getDrawable(WeatherUtils.getIcon(weather.condition.getIcon()))).getBitmap());
            views.setTextViewText(R.id.temp, WeatherUtils.getTemperature(weather.temperature.getTemperature(), context));

            Intent intent = new Intent(context.getApplicationContext(), WidgetProvider.class);
            intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            views.setOnClickPendingIntent(R.id.widget_view, pendingIntent);
            manager.updateAppWidget(widgetId, views);
        }
    }
}


