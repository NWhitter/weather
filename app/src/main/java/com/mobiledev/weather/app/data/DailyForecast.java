package com.mobiledev.weather.app.data;

import com.mobiledev.weather.app.data.interfaces.*;

import java.io.Serializable;

/**
 * @author Natasha Whitter
 * @since 19/05/2014
 */
public class DailyForecast implements Serializable {
    public Condition condition;
    public Temperature temperature;
    public Wind wind;
    public Rain rain;
    public Snow snow;
    public Clouds clouds;

    @Override
    public String toString() {
        return "Forecast{" +
                "condition=" + condition +
                ", temperature=" + temperature +
                ", wind=" + wind +
                ", rain=" + rain +
                ", snow=" + snow +
                ", clouds=" + clouds +
                '}';
    }

    public DailyForecast() {
        condition = new Condition();
        condition = new Condition();
        temperature = new Temperature();
        wind = new Wind();
        rain = new Rain();
        snow = new Snow();
        clouds = new Clouds();
    }

    public class Condition extends BaseCondition implements Serializable{
        private long datetime;

        public long getDatetime() {
            return datetime;
        }

        public void setDatetime(long datetime) {
            this.datetime = datetime;
        }

        @Override
        public String toString() {
            return "Condition{" +
                    "id=" + id +
                    ", condition='" + condition + '\'' +
                    ", description='" + description + '\'' +
                    ", pressure=" + pressure +
                    ", humidity=" + humidity +
                    '}';
        }
    }

    public class Temperature extends BaseTemperature implements Serializable {
        private double nightTemperature;
        private double eveTemperature;
        private double mornTemperature;

        public double getNightTemperature() {
            return nightTemperature;
        }

        public void setNightTemperature(double nightTemperature) {
            this.nightTemperature = nightTemperature;
        }

        public double getEveTemperature() {
            return eveTemperature;
        }

        public void setEveTemperature(double eveTemperature) {
            this.eveTemperature = eveTemperature;
        }

        public double getMornTemperature() {
            return mornTemperature;
        }

        public void setMornTemperature(double mornTemperature) {
            this.mornTemperature = mornTemperature;
        }

        @Override
        public String toString() {
            return "Temperature{" +
                    "temperature=" + temperature +
                    ", minTemperature=" + minTemperature +
                    ", maxTemperature=" + maxTemperature +
                    ", nightTemperature=" + nightTemperature +
                    ", eveTemperature=" + eveTemperature +
                    ", mornTemperature=" + mornTemperature +
                    '}';
        }
    }

    public class Wind extends BaseWind implements Serializable{
    }

    public class Rain extends BaseRain implements Serializable{
    }

    public class Snow extends BaseSnow implements Serializable{
    }

    public class Clouds extends BaseClouds implements Serializable{
    }
}
