package com.mobiledev.weather.app.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobiledev.weather.app.ApplicationConstants;
import com.mobiledev.weather.app.R;
import com.mobiledev.weather.app.data.DailyForecast;
import com.mobiledev.weather.app.data.DailyForecastWeather;
import com.mobiledev.weather.app.utils.WeatherUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @author Natasha Whitter
 * @since 19/05/2014
 */
public class DailyForecastAdapter extends BaseAdapter {

    private DailyForecastWeather weather;
    private Context context;
    private Calendar calendar = Calendar.getInstance();

    public DailyForecastAdapter(Context context, DailyForecastWeather weather) {
        this.context = context;
        this.weather = weather;
    }

    @Override
    public int getCount() {
        if (weather != null)
        {
            return weather.forecasts.size();
        }
        return 0;
    }

    @Override
    public DailyForecast getItem(int position) {
        if (weather != null)
        {
            return weather.forecasts.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (weather != null)
        {
            return position;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.forecast_list_item, null);
        }

        DailyForecast forecast = weather.forecasts.get(position);

        RelativeLayout rlList = (RelativeLayout) convertView.findViewById(R.id.list);
        ImageView icon = (ImageView) convertView.findViewById(R.id.icon);
        TextView tvTitle = (TextView) convertView.findViewById(R.id.time);
        TextView tvTemp = (TextView) convertView.findViewById(R.id.temp);
        TextView tvWindSpeed = (TextView) convertView.findViewById(R.id.wind_speed);

        if (position == 0)
        {
            calendar = Calendar.getInstance();
        }

        rlList.setBackgroundColor(WeatherUtils.getBackgroundColor(forecast.temperature.getTemperature(), context));
        icon.setImageResource(WeatherUtils.getIcon(forecast.condition.getIcon()));
        SimpleDateFormat dateFormat = new SimpleDateFormat(ApplicationConstants.DEFAULT_DATE);
        tvTitle.setText(dateFormat.format(calendar.getTime()));
        calendar.add(Calendar.DAY_OF_MONTH, 1);

        tvTemp.setText(WeatherUtils.getTemperature(forecast.temperature.getTemperature(), context));
        tvWindSpeed.setText(WeatherUtils.getWindSpeed(forecast.wind.getSpeed(), context));

        return convertView;
    }
}
