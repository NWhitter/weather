package com.mobiledev.weather.app.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.*;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;
import com.mobiledev.weather.app.ApplicationConstants;
import com.mobiledev.weather.app.R;
import com.mobiledev.weather.app.ui.CurrentWeatherFragment;
import com.mobiledev.weather.app.utils.LocationUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author Natasha Whitter
 * @since 10/05/2014
 */
public class WeatherClient extends BroadcastReceiver implements LocationListener {
    private static final String TAG = "WeatherClient";
    private Location location;
    private Context context;

    /**
     * Receive location from location manager or geocoder, then get the longitude and latitude from the location. This
     * is send to the corresponding parser class
     */
    public void getLocation()
    {
        try
        {
            if (ApplicationConstants.isNetworkConnected(context))
            {
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
                String selectedLocation = sharedPref.getString(context.getString(R.string.pref_selected_location), "");
                boolean useCurrentLocation = sharedPref.getBoolean(context.getString(R.string.pref_current_location_key), true);
                int classNum = sharedPref.getInt("weatherType", 0);

                double longitude;
                double latitude;
                if (useCurrentLocation || selectedLocation.equals("")) {

                    Log.d(TAG, "Getting Current Location");
                    LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

                    if (manager == null) {
                        return;
                    }

                    String provider = manager.getBestProvider(getCriteria(), true);
                    manager.requestLocationUpdates(provider, 1000, 100, this);

                    Location location = manager.getLastKnownLocation(provider);

                    if (location == null) {
                        return;
                    }

                    Log.d(TAG, location.toString());

                    longitude = location.getLongitude();
                    latitude = location.getLatitude();
                }
                else
                {
                    Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                    Address address = geocoder.getFromLocationName(selectedLocation, 1).get(0);
                    Log.d(TAG, address.toString());
                    longitude = address.getLongitude();
                    latitude = address.getLatitude();
                    Log.d(TAG, Double.toString(longitude) + Double.toString(latitude));
                }

                if (classNum == 0) // 0 = CurrentWeatherFragment
                {
                    Log.d(TAG, "Use CurrentWeatherParser");
                    CurrentWeatherParser parser = new CurrentWeatherParser();
                    parser.execute(longitude, latitude);
                }
                else if (classNum == 1) // 1 = DailyForecastFragment
                {
                    Log.d(TAG, "Use DailyForecastParser");
                    DailyForecastParser parser = new DailyForecastParser();
                    parser.execute(longitude, latitude);
                }
                else if (classNum == 2) // 2 = HourlyForecastFragment
                {
                    Log.d(TAG, "Use HourlyForecastParser");
                    HourlyForecastParser parser = new HourlyForecastParser();
                    parser.execute(longitude, latitude);
                }
                else if (classNum == 3) // 3 = WidgetProvider
                {
                    Log.d(TAG, "Use WidgetWeatherParser");
                    WidgetWeatherParser parser = new WidgetWeatherParser();
                    parser.execute(longitude, latitude);
                }
            }
            else
            {
                Log.d(TAG, "Location unavailable");
                CurrentWeatherFragment.loadingDialog.dismiss();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
            Log.d(TAG, "Can't get current location");
        }
    }

    private Criteria getCriteria()
    {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.NO_REQUIREMENT);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        criteria.setSpeedRequired(true);
        return criteria;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "Received WeatherClient");
        this.context = context;
        if (ApplicationConstants.isNetworkConnected(context))
        {
            getLocation();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (LocationUtils.isBestLocation(location, this.location)) {
            this.location = location;
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
    }
}
