package com.mobiledev.weather.app.network;

import android.os.AsyncTask;
import android.util.Log;
import com.mobiledev.weather.app.data.DailyForecastWeather;
import com.mobiledev.weather.app.network.interfaces.AsyncResponse;
import com.mobiledev.weather.app.network.utils.ParserUtils;

/**
 * @author Natasha Whitter
 * @since 18/05/2014
+ */
public class DailyForecastParser extends AsyncTask<Double, Void, DailyForecastWeather>
{
    private static final String TAG = "DailyForecastParser";
    public static AsyncResponse response = null;

    @Override
    protected DailyForecastWeather doInBackground(Double... params) {

        DailyForecastWeather weather = getWeather(params[0], params[1]);
            Log.d(TAG, weather.toString());
        return weather;
    }

    public static DailyForecastWeather getWeather(double longitude, double latitude)
    {
        return ParserUtils.getDailyForecast(new WeatherConnection().getDailyForecast(longitude, latitude));
    }

    @Override
    protected void onPostExecute(DailyForecastWeather weather) {
        if (response != null)
        {
            response.processFinish(weather);
        }
    }
}
