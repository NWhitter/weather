package com.mobiledev.weather.app.network.interfaces;

import com.mobiledev.weather.app.data.CurrentWeather;
import com.mobiledev.weather.app.ui.widget.WidgetProvider;

/**
 * @author Natasha Whitter
 * @since 20/05/2014
 */
public interface AsyncWidget {
    void processFinish(CurrentWeather weather);
}
