package com.mobiledev.weather.app.data.interfaces;

import java.io.Serializable;

/**
 * @author Natasha Whitter
 * @since 20/05/2014
 */
public abstract class BaseWind implements Serializable{
    protected double speed;
    protected double degree;

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getDegree() {
        return degree;
    }

    public void setDegree(double degree) {
        this.degree = degree;
    }

    @Override
    public String toString() {
        return "Wind{" +
                "speed=" + speed +
                ", degree=" + degree +
                '}';
    }
}
