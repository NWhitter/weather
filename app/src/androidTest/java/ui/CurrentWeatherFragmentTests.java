package ui;

import android.test.ActivityInstrumentationTestCase2;
import com.mobiledev.weather.app.nav.NavigationDrawer;
import com.mobiledev.weather.app.ui.CurrentWeatherFragment;
import com.robotium.solo.Solo;

/**
 * @author Natasha Whitter
 * @since 21/05/2014
 */
public class CurrentWeatherFragmentTests extends ActivityInstrumentationTestCase2<NavigationDrawer>
{
    private NavigationDrawer activity;
    private Solo solo;

    public CurrentWeatherFragmentTests()
    {
        super(NavigationDrawer.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        activity = getActivity();
        solo = new Solo(getInstrumentation(), activity);
    }

    public void testCurrentWeatherFragmentIsShown()
    {
        solo.waitForDialogToClose();
        assertEquals(CurrentWeatherFragment.class, solo.getCurrentActivity().getFragmentManager().findFragmentById(CurrentWeatherFragment.id).getClass());
        solo.setNavigationDrawer(Solo.UP);
        assertEquals(CurrentWeatherFragment.class, solo.getCurrentActivity().getFragmentManager().findFragmentById(CurrentWeatherFragment.id).getClass());
    }

}
